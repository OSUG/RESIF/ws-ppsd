import os


class Config:

    """
    Configure Flask application from environment vars.
    Preconfigured values are set from: RUNMODE=test or RUNMODE=production
    Each parameter can be overriden directly by an environment variable.
    """

    RUNMODE = os.environ.get("RUNMODE")
    if RUNMODE == "production":
        NPZ_DIR = "/mnt/nfs/summer/validated_seismic_metadata/ppsd/"
        PQLX_DIR = "/mnt/auto/archive/metadata/portalproducts/pqlx/"
    elif RUNMODE == "test":
        NPZ_DIR = "/mnt/nfs/summer/validated_seismic_metadata/ppsd/"
        PQLX_DIR = "/mnt/auto/archive/metadata/portalproducts/pqlx/"

    try:
        NPZ_DIR = os.environ.get("NPZ_DIR") or NPZ_DIR
        PQLX_DIR = os.environ.get("PQLX_DIR") or PQLX_DIR
    except NameError:
        print(
            "Missing environment variables. Either RUNMODE=(test|production) or NPZ_DIR and PQLX_DIR should be set."
        )
        raise
