# Webservice ppsd

This service queries a database of hourly segmented power spectral densities (PSDs). Individual PSDs are aggregated to show probabilistic distributions of the seismic noise levels. PSDs are calculated by the method described by McNamara, 2004 using ObsPy for a particular network/station/location/channel combination.

## Query usage

    /query? (channel-options) (date-range-options) [plot-options] [output-options] [nodata=404]

    where:

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    date-range-options   ::  (starttime=<date|duration>) (endtime=<date|duration>)
    plot-options         ::  [dpi=<dots per inch>] [width=<pixels>] [height=<pixels>] [coverage=<true|false>] [cumulative=<true|false>] [spectrogram=<true|false>]
    output-options       ::  [format=<plot|npz>]

    (..) required
    [..] optional

## Sample queries

<a href="http://ws.resif.fr/resifws/ppsd/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&starttime=2020-01-01&endtime=2021-01-01&nodata=404">http://ws.resif.fr/resifws/ppsd/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&starttime=2020-01-01&endtime=2021-01-01&nodata=404</a>

## Detailed descriptions of each query parameter

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name.                                                            |
| sta[tion]  | CIEL     | Station name.                                                                    |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes.                              |
| cha[nnel]  | HHZ      | Channel Code.                                                                    |
| width      | 800      | Width of the figure. Between 640 and 2000. Defaults to 900.                      |
| height     | 600      | Height of the figure. Between 480 and 2000. Defaults to 600.                     |
| dpi        | 150      | Resolution of the figure. Defaults to 100.                                       |
| cumulative | true     | Cumulative version of the histogram.                                             |
| coverage   | false    | Display the data coverage below the PPSD histogram. Only years that are included in the user selection are represented. Green patches represent available data and red patches represent gaps in streams. The bottom row in blue shows psd measurements that are actually present into the histogram whereas gray patches show unselected data. |
| spectrogram | true     | Plot the temporal evolution of the PSDs in a spectrogram-like plot. |
| format     | plot     | Output format. Must be: __plot__ for the graphical output or __npz__ for the compressed numpy binary format |

### Date-range options

| Parameter   | Example             | Discussion                                         |
| :---------- | :------------------ | :------------------------------------------------- |
| start[time] | 2010-01-10T00:00:00 | Selects power spectral density on or after the specified start time. |
| end[time]   | 2011-02-11T01:00:00 | Selects power spectral density on or before the specified end time.  |

The definition of the time interval may take different forms:

  - a calendar dates, for example starttime=2015-08-12T01:00:00
  - duration in seconds, for example endtime=7200
  - the key word "currentutcday" which means midnight of today’s date (UTC time), for example starttime=currentutcday

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

