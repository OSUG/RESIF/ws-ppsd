# Webservice ppsd

Ce service interroge une base de densités spectrales de puissance (PSDs) calculées sur des segments d'une heure. Les PSDs individuelles sont agrégées pour montrer la distribution en probabilité des niveaux de bruits sismiques. Les PSDs sont calculées avec la méthode décrite par McNamara en 2004 en utilisant ObsPy pour une combinaison particulière network/station/location/channel.

## Utilisation de la requête

    /query? (channel-options) (date-range-options) [plot-options] [output-options] [nodata=404]

    où :

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    date-range-options   ::  (starttime=<date|durée>) (endtime=<date|durée>)
    plot-options         ::  [dpi=<dots per inch>] [width=<pixels>] [height=<pixels>] [coverage=<true|false>] [cumulative=<true|false>] [spectrogram=<true|false>]
    output-options       ::  [format=<plot|npz>]

    (..) requis
    [..] optionnel

## Exemples de requêtes

<a href="http://ws.resif.fr/resifws/ppsd/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&starttime=2020-01-01&endtime=2021-01-01&nodata=404">http://ws.resif.fr/resifws/ppsd/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&starttime=2020-01-01&endtime=2021-01-01&nodata=404</a>

## Descriptions détaillées de chaque paramètre de la requête

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique.                                                       |
| sta[tion]  | CIEL    | Nom de la station.                                                            |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides.  |
| cha[nnel]  | HHZ     | Code de canal.                                                                |
| width      | 800     | Largeur de l'image de sortie (pixels). De 640 à 2000. Par défaut à 900.         |
| height     | 600     | Hauteur de l'image de sortie (pixels). De 480 à 2000. Par défaut à 600.         |
| dpi        | 150     | Résolution du graphique. Par défaut à 100.                                      |
| cumulative | true    | Version cumulative de l'histogramme.                                            |
| coverage   | false   | Affiche la couverture des données en dessous de l'histogramme des PPSD. Seules les années qui sont incluses dans la sélection de l'utilisateur sont représentées. Les régions vertes représentent les données disponibles. Les régions rouges représentent les trous dans les séries temporelles. La ligne du bas en bleu montre les mesures qui sont présentent dans l'histogramme alors que les zones grises montrent les données non sélectionnées. |
| spectrogram | true   | Affiche l'évolution temporelle des PSDs sous forme de spectrogramme. |
| format     | plot    | Format de sortie du fichier : __plot__ pour la sortie graphique ou __npz__ pour le format binaire compressé de numpy. |

### Choix de l'intervalle de temps

| Paramètre  | Exemple | Discussion                                                                  |
| :--------- | :------ | :-------------------------------------------------------------------------- |
| start[time] | 2010-01-10T00:00:00 | Sélectionne les densités spectrales de puissance à partir de l'heure spécifiée incluse. |
| end[time]  | 2011-02-11T01:00:00 | Sélectionne les densités spectrales de puissance avant l'heure spécifiée incluse.        |

La définition de l'intervalle de temps avec starttime et endtime peut prendre différentes formes :

  - une date, par exemple starttime=2015-08-12T01:00:00
  - une durée en secondes, par exemple endtime=7200
  - le mot-clé "currentutcday" qui signifie minuit de la date du jour (UTC), par exemple starttime=currentutcday

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

