# limitations
TIMEOUT = 120
MAX_DAYS = 100000

# available parameter values
OUTPUT = ("npz", "plot")
NODATA_CODE = ("204", "404")
STRING_TRUE = ("yes", "true", "t", "y", "1", "")
STRING_FALSE = ("no", "false", "f", "n", "0")

# plots constants
PLOT_COLOR = "155084"
XTICKS_ROTATION = 0

HEIGHT = 600
HEIGHT_MIN = 480
HEIGHT_MAX = 2000

WIDTH = 900
WIDTH_MIN = 640
WIDTH_MAX = 2000

DPI = 100
DPI_MIN = 75
DPI_MAX = 300

# error message constants
DOCUMENTATION_URI = "http://ws.resif.fr/resifws/ppsd/1/"
SERVICE = "resifws-ppsd"
VERSION = "1.0.0"


class Error:
    UNKNOWN_PARAM = "Unknown query parameter: "
    MULTI_PARAM = "Multiple entries for query parameter: "
    VALID_PARAM = "Valid parameters."
    START_LATER = "The starttime cannot be later than the endtime: "
    TOO_LONG_DURATION = "Too many days requested (greater than "
    UNSPECIFIED = "Error processing your request."
    NODATA = "Your query doesn't match any available data."
    TIMEOUT = f"Your query exceeds timeout ({TIMEOUT} seconds)."
    MISSING = "Missing parameter: "
    BAD_VAL = " Invalid value: "
    CHAR = "White space(s) or invalid string. Invalid value for: "
    EMPTY = "Empty string. Invalid value for: "
    BOOL = "(Valid boolean values are: true/false, yes/no, t/f or 1/0)"
    NETWORK = "Invalid network code: "
    STATION = "Invalid station code: "
    LOCATION = "Invalid location code: "
    CHANNEL = "Invalid channel code: "
    TIME = "Bad date value: "
    INT_BETWEEN = "must be an integer between"
    DPI = f"dpi {INT_BETWEEN} {DPI_MIN} and {DPI_MAX}." + BAD_VAL
    WIDTH = f"width {INT_BETWEEN} {WIDTH_MIN} and {WIDTH_MAX}." + BAD_VAL
    HEIGHT = f"height {INT_BETWEEN} {HEIGHT_MIN} and {HEIGHT_MAX}." + BAD_VAL
    COLOR = "Invalid HEX color code (must be such as B22222)." + BAD_VAL
    OUTPUT = f"Accepted format values are: {OUTPUT}." + BAD_VAL
    PROCESSING = "Your request cannot be processed. Check for value consistency."
    NODATA_CODE = f"Accepted nodata values are: {NODATA_CODE}." + BAD_VAL
    NO_SELECTION = "Request contains no selections."
    MISSING_OUTPUT = "Missing format parameter."
    NO_WILDCARDS = "Wildcards or list are not allowed." + BAD_VAL


class HTTP:
    _200_ = "Successful request. "
    _204_ = "No data matches the selection. "
    _400_ = "Bad request due to improper value. "
    _401_ = "Authentication is required. "
    _403_ = "Forbidden access. "
    _404_ = "No data matches the selection. "
    _408_ = "Request exceeds timeout. "
    _413_ = "Request too large. "
    _414_ = "Request URI too large. "
    _500_ = "Internal server error. "
    _503_ = "Service unavailable. "
