import calendar
import glob
import io
import logging
import os
import re
import time
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile
from zipfile import ZipFile, ZIP_DEFLATED

import matplotlib.pyplot as plt
from flask import current_app, make_response
from obspy import UTCDateTime
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx

from apps.globals import DPI
from apps.globals import HEIGHT
from apps.globals import WIDTH
from apps.globals import Error
from apps.utils import error_request
from apps.utils import tictac
from apps.plot_spectrogram import plot_spectrogram

try:
    plt.switch_backend("WebAgg")
except Exception:
    pass


def get_nslc(params):
    cid = f"{params['network']}.{params['station']}.{params['location']}.{params['channel']}"
    return cid.replace("--", "")


def npz_paths(params):
    paths = []
    npz_dir = current_app.config["NPZ_DIR"]
    years = range(params["start"].year, params["end"].year + 1)
    cid = get_nslc(params)
    for year in years:
        for quality in [".bud", ""]:
            filename = f"{cid}.D.{year}{quality}.npz"
            path = os.path.join(npz_dir, params["network"], str(year), filename)
            if os.path.isfile(path):
                paths.append(path)
    return paths


def make_subtitle(paths):
    now = time.time()
    subtitle = "PSDs calculation time: "
    for path in paths:
        ptime = os.path.getctime(path)
        if ptime < now:
            now = ptime
    subtitle = subtitle + datetime.fromtimestamp(now).strftime("%Y-%m-%d")
    for path in paths:
        if "bud" in path:
            subtitle = subtitle + " (contains calculations performed on raw data)"
            break
    return subtitle


def is_outadated(paths):
    for path in paths:
        fname = path.replace(".bud", "").replace("npz", "dirty")
        if os.path.exists(fname):
            return True


def ppsd_plot(params, paths):
    ppsd = None
    for path in paths:
        try:
            if not ppsd:
                ppsd = PPSD.load_npz(path)
            else:
                ppsd.add_npz(path)
        except Exception:
            pass

    try:
        start, end = UTCDateTime(params["start"]), UTCDateTime(params["end"])
        ppsd.calculate_histogram(starttime=start, endtime=end)
        start_used, end_used = ppsd.current_times_used[0], ppsd.current_times_used[-1]
        start_used = start_used.isoformat()
        end_used = (end_used + timedelta(seconds=ppsd.ppsd_length)).isoformat()
    except Exception:
        return

    logging.info("Using matplotlib backend: %s", plt.get_backend())
    if params["spectrogram"]:
        ind = [n for n, t in enumerate(ppsd.times_processed) if start <= t <= end]
        ppsd._times_processed = [ppsd._times_processed[i] for i in ind]
        ppsd._binned_psds = [ppsd._binned_psds[i] for i in ind]
        fig = plot_spectrogram(ppsd, cmap=pqlx, show=False)
    else:
        fig = ppsd.plot(
            show_coverage=params["coverage"],
            cumulative=params["cumulative"],
            cmap=pqlx,
            show=False,
        )

    if is_outadated(paths):
        plt.annotate(
            "OUTDATED calculations",
            xy=(0.7, 0.05),
            xycoords="figure fraction",
            style="oblique",
            color="red",
            weight="bold",
            bbox={"facecolor": "yellow", "alpha": 0.5, "pad": 4},
        )

    width, height = (params["width"] or WIDTH) / DPI, (params["height"] or HEIGHT) / DPI
    fig.set_size_inches(width, height)
    fig.axes[0].set_title(make_subtitle(paths), fontsize=9)
    fig.suptitle(f"{ppsd.id}  {start_used} -- {end_used}", fontsize=11)
    plt.tight_layout(rect=[0.08, 0.1, 1.0, 0.95])

    img = io.BytesIO()
    fig.savefig(img, format="png", dpi=params["dpi"] or DPI)
    plt.close()
    return img


def nodata_error(params):
    code = int(params["nodata"])
    return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)


def monthdelta(date, delta):
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m:
        m = 12
    d = min(date.day, calendar.monthrange(y, m)[1])
    return date.replace(day=d, month=m, year=y)


def pqlx_image_name(params):
    now = datetime.now()
    if params["start"] > now:
        ext = "ALL"
    elif params["start"] > now - timedelta(days=1):
        ext = "YDAY"
    elif params["start"] > now - timedelta(days=2):
        ext = "LYDAY"
    elif params["start"] > now - timedelta(weeks=1):
        ext = "WEEK"
    elif params["start"] > now - timedelta(weeks=2):
        ext = "LWEEK"
    elif params["start"] > monthdelta(now, -1):
        ext = "MONTH"
    elif params["start"] > monthdelta(now, -2):
        ext = "LMONTH"
    elif params["start"] > monthdelta(now, -12):
        ext = "YEAR"
    elif params["start"] > monthdelta(now, -24):
        ext = "LYEAR"
    else:
        ext = "ALL"
    nslc = get_nslc(params)
    spec = ".SP" if params["spectrogram"] else ""
    return f"{nslc}.{ext}{spec}.png"


def get_output(params):
    """Builds the ws-ppsd response
    params: parameters"""

    response = None
    tic = time.time()
    try:
        if re.match("[0-9XYZ]", params["network"]):
            pqlx_dir = current_app.config["PQLX_DIR"]
            net = params["network"][:2]
            search_path = os.path.join(pqlx_dir, net)
            paths = sorted(glob.glob(f"{search_path}*"), reverse=True)
            if not paths:
                return nodata_error(params)
            for path in paths:
                if int(os.path.split(path)[-1][2:]) <= params["end"].year:
                    break
            imagename = os.path.join(path, pqlx_image_name(params))
            if not os.path.exists(imagename):
                return nodata_error(params)
            with open(imagename, "rb") as imagefile:
                img = imagefile.read()
            response = make_response(img, {"Content-type": "image/png"})
        else:
            paths = npz_paths(params)
            logging.debug(paths)
            if not paths:
                return nodata_error(params)

            if params["format"] == "plot":
                img = ppsd_plot(params, paths)
                if not img:
                    return nodata_error(params)
                headers = {"Content-type": "image/png"}
                response = make_response(img.getvalue(), headers)
            elif params["format"] == "npz":
                tmp_zip = NamedTemporaryFile()
                fname = "resifws-ppsd"
                with ZipFile(tmp_zip.name, "w", ZIP_DEFLATED) as fzip:
                    for path in paths:
                        fzip.write(path, os.path.basename(path))
                headers = {"Content-Disposition": f"attachment; filename={fname}.zip"}
                response = make_response(tmp_zip.read(), headers)
                response.headers["Content-type"] = "application/x-zip-compressed"

        if response:
            nbytes = response.headers.get("Content-Length")
            logging.info("%s bytes rendered in %s seconds.", nbytes, tictac(tic))
        return response
    except Exception as ex:
        logging.exception(str(ex))
