import matplotlib.pyplot as plt
import numpy as np

from obspy.imaging.cm import pqlx
from obspy.imaging.util import _set_xaxis_obspy_dates


def _split_lists(ppsd, times, psds):
    """ """
    t_diff_gapless = ppsd.step * 1e9
    gap_indices = np.argwhere(np.diff(times) - t_diff_gapless)
    gap_indices = (gap_indices.flatten() + 1).tolist()

    if not len(gap_indices):
        return [(times, psds)]

    gapless = []
    indices_start = [0] + gap_indices
    indices_end = gap_indices + [len(times)]
    for start, end in zip(indices_start, indices_end):
        gapless.append((times[start:end], psds[start:end]))
    return gapless


def _get_gapless_psd(ppsd):
    """
    Helper routine to get a list of 2-tuples with gapless portions of
    processed PPSD time ranges.
    This means that PSD time history is split whenever to adjacent PSD
    timestamps are not separated by exactly
    ``ppsd.ppsd_length * (1 - ppsd.overlap)``.
    """
    return ppsd._split_lists(ppsd.times_processed, ppsd.psd_values)


def plot_spectrogram(ppsd, cmap=pqlx, clim=None, grid=True, filename=None, show=True):
    """
    Plot the temporal evolution of the PSD in a spectrogram-like plot.

    .. note::
        For example plots see the :ref:`Obspy Gallery <gallery>`.

    :type cmap: :class:`matplotlib.colors.Colormap`
    :param cmap: Specify a custom colormap instance. If not specified, then
        the default ObsPy sequential colormap is used.
    :type clim: list
    :param clim: Minimum/maximum dB values for lower/upper end of colormap.
        Specified as type ``float`` or ``None`` for no clipping on one end
        of the scale (e.g. ``clim=[-150, None]`` for a lower limit of
        ``-150`` dB and no clipping on upper end).
    :type grid: bool
    :param grid: Enable/disable grid in histogram plot.
    :type filename: str
    :param filename: Name of output file
    :type show: bool
    :param show: Enable/disable immediately showing the plot.
    """

    fig, ax = plt.subplots()

    yedges = ppsd.period_xedges

    datas = []
    xedges_all = set()
    xedges_old = None
    gapless_ppsd = ppsd._get_gapless_psd()
    empty_array = np.full((len(yedges) - 1, 1), np.nan)

    for times, psds in gapless_ppsd:
        xedges = [t.matplotlib_date for t in times] + [
            (times[-1] + ppsd.step).matplotlib_date
        ]
        data = np.array(psds).T
        xedges_all.update(xedges)
        if xedges_old and xedges[0] != xedges_old[1]:
            datas.append(empty_array)
        datas.append(data)
        xedges_old = xedges

    datas = np.concatenate(datas, axis=1)
    xedges_all = sorted(list(xedges_all))
    meshgrid_x, meshgrid_y = np.meshgrid(xedges_all, yedges)

    if clim is None:
        cmin = min(min(psds[0]) for _, psds in gapless_ppsd)
        cmax = max(max(psds[0]) for _, psds in gapless_ppsd)
        clim = (cmin, cmax)
    else:
        cmin, cmax = clim

    quadmesh = ax.pcolormesh(
        meshgrid_x, meshgrid_y, datas, cmap=cmap, vmin=cmin, vmax=cmax, zorder=-1
    )

    cb = plt.colorbar(quadmesh, ax=ax)

    if grid:
        ax.grid()

    if ppsd.special_handling is None:
        cb.ax.set_ylabel("Amplitude [$m^2/s^4/Hz$] [dB]")
    else:
        cb.ax.set_ylabel("Amplitude [dB]")
    ax.set_ylabel("Period [s]")

    fig.autofmt_xdate()
    _set_xaxis_obspy_dates(ax)

    ax.set_yscale("log")
    ax.set_xlim(
        ppsd.times_processed[0].matplotlib_date,
        (ppsd.times_processed[-1] + ppsd.step).matplotlib_date,
    )
    ax.set_ylim(yedges[0], yedges[-1])
    try:
        ax.set_facecolor("0.8")
    # mpl <2 has different API for setting Axes background color
    except AttributeError:
        ax.set_axis_bgcolor("0.8")

    fig.tight_layout()

    if filename is not None:
        plt.savefig(filename)
        plt.close()
    elif show:
        plt.draw()
        plt.show()
    else:
        plt.draw()
        return fig
