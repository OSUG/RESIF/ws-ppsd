import logging
import queue
import re
from multiprocessing import Process, Queue

from flask import request

from apps.globals import DPI_MIN, DPI_MAX
from apps.globals import Error
from apps.globals import HEIGHT_MIN, HEIGHT_MAX
from apps.globals import HTTP
from apps.globals import MAX_DAYS
from apps.globals import TIMEOUT
from apps.globals import WIDTH_MIN, WIDTH_MAX
from apps.output import get_output
from apps.parameters import Parameters
from apps.utils import check_base_parameters
from apps.utils import check_request
from apps.utils import error_param
from apps.utils import error_request
from apps.utils import is_valid_color
from apps.utils import is_valid_integer
from apps.utils import is_valid_nodata
from apps.utils import is_valid_output


def check_parameters(params):

    # check base parameters
    (params, error) = check_base_parameters(params, MAX_DAYS)
    if error["code"] != 200:
        return (params, error)

    # Plot parameter validations
    if not is_valid_color(params["color"]):
        return error_param(params, Error.COLOR + str(params["color"]))

    if params["width"]:
        if not is_valid_integer(params["width"], WIDTH_MIN, WIDTH_MAX):
            return error_param(params, Error.WIDTH + str(params["width"]))
        params["width"] = int(params["width"])

    if params["height"]:
        if not is_valid_integer(params["height"], HEIGHT_MIN, HEIGHT_MAX):
            return error_param(params, Error.HEIGHT + str(params["height"]))
        params["height"] = int(params["height"])

    if params["dpi"]:
        if not is_valid_integer(params["dpi"], DPI_MIN, DPI_MAX):
            return error_param(params, Error.DPI + str(params["dpi"]))
        params["dpi"] = int(params["dpi"])

    # output parameter validation
    if not is_valid_output(params["format"]):
        return error_param(params, Error.OUTPUT + str(params["format"]))
    params["format"] = params["format"].lower()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        return error_param(params, Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    # wildcards or list are not allowed
    for key in ("network", "station", "location", "channel"):
        if re.search(r"[,*?]", params[key]):
            return error_param(params, Error.NO_WILDCARDS + key)

    for key, val in params.items():
        logging.debug("%s: %s", key, str(val))

    return (params, {"msg": HTTP._200_, "details": Error.VALID_PARAM, "code": 200})


def checks_get():

    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    (p, result) = check_request(params)
    if result["code"] != 200:
        return (p, result)
    return check_parameters(params)


def output():

    try:
        process = None
        result = {"msg": HTTP._400_, "details": Error.UNKNOWN_PARAM, "code": 400}
        logging.debug(request.url)

        (params, result) = checks_get()
        if result["code"] == 200:

            def put_response(q):
                q.put(get_output(params))

            q = Queue()
            process = Process(target=put_response, args=(q,))
            process.start()
            resp = q.get(timeout=TIMEOUT)

            if resp:
                return resp
            else:
                raise Exception

    except queue.Empty:
        result = {"msg": HTTP._408_, "details": Error.TIMEOUT, "code": 408}

    except Exception as excep:
        result = {"msg": HTTP._500_, "details": Error.UNSPECIFIED, "code": 500}
        logging.exception(str(excep))

    finally:
        if process:
            process.terminate()

    return error_request(
        msg=result["msg"], details=result["details"], code=result["code"]
    )
