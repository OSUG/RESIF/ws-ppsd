from apps.globals import PLOT_COLOR


class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.starttime = None
        self.endtime = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.start = None
        self.end = None
        self.width = None
        self.height = None
        self.dpi = None
        self.color = PLOT_COLOR
        self.coverage = "true"
        self.cumulative = "false"
        self.spectrogram = "false"
        self.spec = "false"
        self.format = "plot"
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
                ("starttime", "start"),
                ("endtime", "end"),
                ("spectrogram", "spec"),
            ],
            "booleans": ["coverage", "cumulative", "spectrogram"],
            "floats": [],
            "not_none": ["start"],
        }

    def todict(self):
        return self.__dict__
